import os
import sys
from kyss.setuptools import setup

AUTHOR="Ulukyn"
EMAIL="ulukyn@gmail.com"
PACKAGE_NAME="kyss-webview"
PACKAGE_DESC="Kyss Webview"
PACKAGE_VERSION="1.2"
URL="https://gitlab.com/ulukyn/kyss-webview"
DOWNLOAD="https://gitlab.com/api/v4/projects/27138776/repository/archive.zip?sha=v1.0"
REQUIRE={"Linux" :  ["pywebview[qt]"], "Windows" : ["pywebview[cef]"], "*" : ["pywebview", "chevron", "certifi"]}
LICENSE="GNU General Public License v3.0"

setup(PACKAGE_NAME, PACKAGE_VERSION, PACKAGE_DESC, REQUIRE, URL, DOWNLOAD, AUTHOR, EMAIL, LICENSE)

