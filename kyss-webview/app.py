########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from module import KyssModule
from kyss.utils import KyssUtils
ku = KyssUtils("KyssWebview")

class kyssApp(KyssModule):

	def call_Refresh(self):
		KyssModule.base_url = KyssModule.main_window.resolve_url("file://", should_serve=True)+"/"

	def start(self, params):
		min_width, min_height = (1024,768)

		width = round(float(width))
		height = round(float(height))
		min_width = round(float(min_width))
		min_height = round(float(min_height))

		ku.exec("Create WebView...")
		try:
			window = webview.create_window(params["name"], icon=ku.path("data/icon"), js_api=self, width=width, height=height, min_size=(min_width, min_height))
		except:
			window = webview.create_window(params["name"], js_api=self, width=width, height=height, min_size=(min_width, min_height))

		KyssModule.main_window = window

		if platform.system() == "Windows":
			webview.start(self.call_Refresh, http_server=True, gui=params["windows_gui"], debug=True)
		elif platform.system() == "Linux":
			webview.start(self.call_Refresh, http_server=True, gui=params["linux_gui"], debug=True)
		else:
			webview.start(self.call_Refresh, http_server=True, debug=True)
