########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import io
import os
import sys
import re
import html
import json
import time
import queue
import string
import locale
import codecs
import appdirs
import urllib3
import certifi
import chevron
import platform
import traceback
from pathlib import Path, PureWindowsPath, PurePosixPath

from kyss.utils import KyssUtils
ku = KyssUtils("KyssWebview")

from kyss.config import KyssConfig

### Hack to show chevron undefined partials as untranslated sentences
def get_partial(name, partials_dict, module, partials_ext):
	"""Load a partial"""
	if "." in name:
		module, name = name.split(".")

	try:
		# Maybe the partial is in the dictionary
		return partials_dict[module][name]
	except KeyError:
		return "{"+module+"."+name+"}"

chevron.renderer._get_partial = get_partial

class TemplateFormatter(string.Formatter):
	def format_field(self, value, spec):
		if spec.startswith("repeat:"):
			template = spec.partition(":")[-1]
			return ''.join([template.format(name=name, item=item) for name, item in value.items()])
		elif spec == "call":
			return value()
		elif spec.startswith("cycle:"):
			if value % 2:
				return spec.partition(":")[3]
			return spec.partition(":")[2]
		elif spec.startswith("if:"):
			if value:
				return spec.partition(":")[-1]
			return ""
		elif spec.startswith("ifnot:"):
			if not value:
				return spec.partition(":")[-1]
			return ""
		else:
			return super(TemplateFormatter, self).format_field(value, spec)

